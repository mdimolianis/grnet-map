import requests
import json
from config import *

site_naming_convention = {
        "YPEPTH":"YPE",
        "KOLETTI": "OTE_KOLETTI",
        "ATHENA": "ATHENA_KETEP",
        "KNOSSOS": "KNOSSOS_DC",
        "LOUROS": "LOUROS_DC",
        "NIARCHOS": "NIARCHOS_DC",
        "HMU_RETH": "HMU_PERIVOL"
        }
color_naming_convention = {
        "ge-": "#0000D3", #blue
        "xe-": "#D36D00", #orange  
        "et-": "#00D310"  #green
        }
#Configuration parameters
all_devices = []
headers = {
  'Authorization': 'Basic '+str(Authorization),
  'Content-Type': 'application/json'
  }


def delete_all_devices_from_enms(enms_url):
    request = requests.request("GET", enms_url+"/rest/query/device", headers=headers)
    devices = request.json()
    for device in devices:
        device_name = device["name"]
        request_for_deletion = requests.request("DELETE", enms_url+"/rest/instance/device/"+str(device_name), headers=headers)
        print("Deleting "+device_name)

def post_device_to_enms(enms_url,device_name,latitude,longitude):
    icon = 'router' if "router" in device_role else 'switch'
    # Extensible model based on the enms data model
    payload = json.dumps({
  "name": device_name,
  "latitude": latitude,
  "longitude": longitude,
  "icon": icon,
  "subtype": device_role
   })
    
    response = requests.request("POST", enms_url+"/rest/instance/device", headers=headers, data=payload)
    print(response.text)

def post_link_to_enms(enms_url,device_a,device_b,interface_a,interface_b):
    color="#000000"
    for interface,color_code in color_naming_convention.items():
        if (interface in interface_a) or (interface in interface_b):
            color=color_code
    # Extensible model based on` the enms data model
    payload = json.dumps({
   "name": device_a+"-"+device_b,
   "source_name": device_a,
   "destination_name": device_b,
   "description": interface_a+"-"+interface_b,
   "color": color
   })

    response = requests.request("POST", enms_url+"/rest/instance/link", headers=headers, data=payload)
    print(response.text)

def get_devices_from_netbox(netbox_url,device_role):
#Retrieve devices from netbox
    headers = {'Authorization': "Token"+str(netbox_token) } 
    url = netbox_url + 'dcim/devices/?role='+str(device_role)+"&limit=0"
    print(url)
    req = requests.get(url,headers)
    if req.status_code!=200:
        print("HTTP Response not 200 but "+str(req.status_code))
        print("Stopping")
        exit()
    resp = req.json()
    results = resp["results"]
    return(results)

def get_connections_from_netbox(netbox_url):
#Retrieve devices from netbox
    headers = {'Authorization': "Token"+str(netbox_token) }
    url = netbox_url + 'dcim/interface-connections/?limit=0'
    req = requests.get(url,headers)
    if req.status_code!=200:
        print("HTTP Response not 200 but "+str(req.status_code))
        print("Stopping")
        exit()
    resp = req.json()
    results = resp["results"]
    return(results)

def get_device_coordinates(device):
    print(device["name"],device["site"]["name"])
    device_name = device["name"]
    device_site = device["site"]["name"]
    #if there is a mismatch between netbox and members we need to update the value
    if device_site in site_naming_convention.keys():
        device_site=site_naming_convention[device_site]
    try:
        request_coordinates = requests.get("https://network2.grnet.gr/api/locations/"+str(device_site))
        resp = request_coordinates.json()
        latitude = resp["geo_lat"]
        longitude = resp["geo_lng"]
        #print(latitude,longitude)
        return (device_name,latitude,longitude)
    except:
        print("-------------No site coords for site "+str(device_site))


#Main Program

#Delete devices
delete_all_devices_from_enms(enms_url)

for device_role in device_roles:
    devices=get_devices_from_netbox(netbox_url,device_role)
    print(device_role,len(devices))
    for device in devices:
        try:
            device_name,latitude,longitude = get_device_coordinates(device)
            post_device_to_enms(enms_url,device_name,latitude,longitude)
            all_devices.append(device_name)
        except:
            print("Faulty data for "+str(device["name"]))

connections = get_connections_from_netbox(netbox_url)
for connection in connections:
    device_a    = connection["interface_a"]["device"]["name"]
    device_b    = connection["interface_b"]["device"]["name"]
    interface_a = connection["interface_a"]["name"]
    interface_b = connection["interface_b"]["name"]
    #check that the devices of the connections have been included in the eNMS
    if (device_a in all_devices) and (device_b in all_devices):
        post_link_to_enms(enms_url,device_a,device_b,interface_a,interface_b)
    else:
        missing_device = device_b if device_a in all_devices else device_a
        print("Missing device: "+str(missing_device))

